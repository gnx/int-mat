var params = {
  "appName": "geometry",
  "width": 800,
  "height": 600,
  "showToolBar": true,
  "showAlgebraInput": true,
  "showMenuBar": true,
  "enableFileFeatures": false,
  "useBrowserForJS": true,
  "showFullscreenButton": true,
  "rounding": "2"
  };

function ggbOnInit(param) {
  ggbApplet.setGridVisible(true);
  ggbApplet.setAxesVisible(true, true);
	ggbApplet.evalCommand("(1,3)\n(2,4)\nLine(A,B)");
}

var ggbApplet = new GGBApplet(params, true);

window.addEventListener("load", function() { 
    ggbApplet.inject("ggb-element");
});