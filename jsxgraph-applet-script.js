var b = JXG.JSXGraph.initBoard('jxgbox', {boundingbox: [-5, 10, 15, -10], axis:true, grid:true});
p = b.create('point',[1,4],{size:4,name:'A'});
var dataX = [1,2,3,4,5,6,7,8];
var dataY = [0.3,4.0,-1,2.3,7,9,8,9];
b.create('curve', [dataX,dataY],{strokeColor:'red',strokeWidth:3});
b.create('curve', [dataX,function(x){ return p.X()*Math.sin(x)*x;}],{strokeColor:'blue',strokeWidth:3,dash:1});